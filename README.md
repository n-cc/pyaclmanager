# pyaclmanager

pyaclmanager is a wrapper around [pylibacl](https://github.com/iustin/pylibacl) to make interaction with ACL-capable files and directories easier. It is still in pre-alpha and subject to many changes, including the name of the project.
