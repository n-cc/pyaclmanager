import typing
from pathlib import Path

import posix1e


class ACLPath:
    def __init__(self, path: typing.Union[str, Path]):
        if type(path) is str:
            path = Path(path)

        self._path_object = path

        if not (self._path_object.is_file() or self._path_object.is_dir()):
            raise FileNotFoundError(str(self._path_object))

        self._acl = posix1e.ACL(file=str(self._path_object.resolve()))
